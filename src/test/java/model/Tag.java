package model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Tag {
    private Integer id;
    private String name;

    public Tag() {
        setId(32);
        setName("Jessy");
    }

    public Tag(Integer id, String name) {
        setId(id);
        setName(name);
    }

    public Tag(String tagString) {
        setId(Integer.parseInt(tagString.split(":")[0]));
        setName(tagString.split(":")[1]);
    }
}
