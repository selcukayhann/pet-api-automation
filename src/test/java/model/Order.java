package model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Order {

    private Integer id;
    private int petId;
    private int quantity;
    private String shipDate;
    private String status;
    private boolean complete;

    public Order() {
        setId(32);
        setPetId(3);
        setQuantity(2);
        setShipDate("2019-02-28T14:12:44.922Z");
        setStatus("placed");
        setComplete(false);
    }

    public Order(Integer id, int petId, int quantity, String shipDate, String status, boolean complete) {
        setId(id);
        setPetId(petId);
        setQuantity(quantity);
        setShipDate(shipDate);
        setStatus(status);
        setComplete(complete);
    }
}
