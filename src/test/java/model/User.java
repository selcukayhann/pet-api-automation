package model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class User{

        private int id;
        private String username;
        private String firstname;
        private String lastname;
        private String email;
        private String password;
        private String phone;
        private int userStatus;

        public User() {
            setId(19);
            setUsername("testuser");
            setFirstname("test");
            setLastname("user");
            setEmail("test.user@testing.com");
            setPassword("password123");
            setPhone("07898889878");
            setUserStatus(0);
        }

        public User(int id, String username, String firstname, String lastname, String email, String password, String Phone, int userStatus) {
            setId(19);
            setUsername(username);
            setFirstname(firstname);
            setLastname(lastname);
            setEmail(email);
            setPassword(password);
            setPhone(phone);
            setUserStatus(userStatus);
        }
}
