package model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Category {
    private Integer id;
    private String name;

    public Category() {
        setId(32);
        setName("Jessy");
    }

    public Category(Integer id, String name) {
        setId(id);
        setName(name);
    }

    public Category(String categoryString) {
        setId(Integer.parseInt(categoryString.split(":")[0]));
        setName(categoryString.split(":")[1]);
    }
}
