package model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;

@Getter @Setter
public class Pet {
    private Integer id;
    private Category category;
    private String name;
    private String[] photoUrls;
    private Tag[] tags;
    private String status;

    public Pet() {
        setId(29);
        setCategory(new Category(13, "Cainine"));
        setName("Wolf");
        setPhotoUrls(new String[] {"http://pathtoimage1"});
        setTags(new Tag[] {new Tag(17, "Furry")});
        setStatus("available");
    }

    public Pet(Integer id, Category category, String name, String[] photoUrls, Tag[] tags, String status) {
        setId(id);
        setCategory(category);
        setName(name);
        setPhotoUrls(photoUrls);
        setTags(tags);
        setStatus(status);
    }

    public Pet(Integer id, String categoryString, String name, String photoUrlsString, String tagsString,
               String status) {
        String[] categoryStringArray = categoryString.split(":");

        Pair<String, String> category = Pair.of(categoryStringArray[0], categoryStringArray[1]);

        String[] photoUrlsArray = photoUrlsString.split(":");

        ArrayList<Pair<Integer, String>> tagsList = new ArrayList<Pair<Integer, String>>();

        if (!"".equalsIgnoreCase(tagsString)) {
            String[] tagStringArray = tagsString.split(",");
            for (int i = 0; i < tagStringArray.length; i++) {
                String[] tagArray = tagStringArray[i].split(":");
                tagsList.add(Pair.of(Integer.parseInt(tagArray[0]), tagArray[1]));
            }
        }

        Category cat = new Category(Integer.parseInt(category.getKey()), category.getValue());

        Tag[] tagArray = new Tag[tagsList.size()];
        for (int i = 0; i < tagsList.size(); i++) {
            Pair<Integer, String> p = tagsList.get(i);
            tagArray[i] = new Tag(p.getKey(), p.getValue());
        }

        setId(id);
        setCategory(cat);
        setName(name);
        setPhotoUrls(photoUrlsArray);
        setTags(tagArray);
        setStatus(status);
    }
}
