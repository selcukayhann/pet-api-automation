package steps;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import model.Pet;

@Getter @Setter @NoArgsConstructor
public class Session {
    private Response response;
    private RequestSpecification request;
    private Pet pet;

}
