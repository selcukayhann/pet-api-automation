package steps;

import api.BaseService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CommonDefinitions {

    @Steps
    private Session session;

    @Steps
    private BaseService service;

    public CommonDefinitions(Session session) {
        this.session = session;
    }

    public CommonDefinitions(){}

    @Given("the Swagger Petstore API is available")
    public void the_swagger_petstore_api_is_available() {
        String url = service.getBaseUrl() + "swagger.json";
        service.sendRequest(null, BaseService.GET_REQUEST, url, null).then().statusCode(200);
    }

    @Then("^the requests response will contain the value '(.*?)' for the '(.*?)' field$")
    public void i_will_be_able_to_run_connected_step_definitions(String val, String key) {
        service.verifyResponseKeyValues(key, val, session.getResponse());
    }
}
