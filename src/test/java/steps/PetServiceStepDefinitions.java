package steps;

import api.PetService;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;
import model.Category;
import model.Pet;
import model.Tag;
import net.thucydides.core.annotations.Steps;

import java.util.List;


public class PetServiceStepDefinitions {

    @Steps
    private Session session;

    @Steps
    private PetService petService;

    public PetServiceStepDefinitions(){ }

    public PetServiceStepDefinitions(Session session){
        this.session = session;
    }


    @When("^I add a Pet to the system$")
    public void i_add_a__pet_to_the_system() {
        petService.addPet(session);
    }

    @Then("^the pet request response has a '(\\d+)' response code$")
    public void the_pet_request_response_has_the_correct_response_code(Integer rc) {
        petService.verifyResponseStatusValue(session.getResponse(), rc.intValue());
    }

    @Then("^the pet requests response contains the correct json data$")
    public void the_pet_requests_response_contains_the_correct_json_data() {
        petService.verifyPetValuesAreAsExpected(session.getResponse(), petService.getDefaultPet());
    }

    @Given("^a pet exists$")
    public void a_pet_exists() {
        petService.addPet(session);
        petService.verifyResponseStatusValue(session.getResponse(), petService.SUCCESS_STATUS_CODE);
    }

    @When("^I delete the pet$")
    public void i_delete_the_pet() {
        session.setResponse(petService.deletePet(session.getRequest()));
    }

    @When("^then search for the pet by it's id$")
    public void then_search_for_the_pet_by_its_id() {
        session.setResponse(petService.getPetById(session.getRequest()));
    }

    @Given("^a cat is '(.*?)'$")
    public void a_cat_is_availablilability(String availability) {
        petService.addPet(session,
                new Pet(16, "7:feline", "Pussy Cat", "image1:image2", "17:Furry", availability));
    }

    @Then("^I can add a pet that has multiple tags$")
    public void i_can_add_a_pet_that_has_multiple_tags() {
        petService.addPet(session, new Pet(16, "45:rodent", "Rat", "image1", "17:Furry,29:cute,33:Small", "available"));
        petService.verifyResponseStatusValue(session.getResponse(), petService.SUCCESS_STATUS_CODE);
    }

    @Then("^I can add a pet that has no tags$")
    public void i_can_add_a_pet_that_has_no_tags() {
        petService.addPet(session, new Pet(16, "45:rodent", "Rat", "image1", "", "available"));
        petService.verifyResponseStatusValue(session.getResponse(), petService.SUCCESS_STATUS_CODE);
    }

    @When("^I add a pet to the system without providing an id value$")
    public void i_add_a_pet_to_the_system_without_providing_an_id_value() {
        petService.addPet(session,
                new Pet(null, "45:rodent", "Rat", "image1", "17:Furry,29:cute,33:Small", "available"));
    }

    @Then("^an id is automatically generated for the added pet$")
    public void an_id_is_automatically_generated_for_the_added_pet() {
        petService.verifyPetHasAnId(session.getResponse());
    }

    @When("^I add a pet and the json body is malformed and consists of only '(.*?)'$")
    public void i_add_a_pet_and_the_json_body_is_malformed(String body) {
        session.setRequest(petService.getRequestWithJSONHeaders());
        session.setResponse(petService.addPetWithBody(session.getRequest(), body));
    }

    @When("I add a Pet to the system with id, category, name, photoUrls, tags, status")
    public void iAddAPetToTheSystemWithIdCategoryNamePhotoUrlsTagsStatus(DataTable dt) {
        List<String> petSpecs = dt.asList();
        String[] photoUrls = petSpecs.get(3).split(",");
        String[] tagsStringArray = petSpecs.get(4).split(",");
        Tag[] tags = new Tag[tagsStringArray.length-1];
        for (int i=0; i<tags.length;i++) {
            tags[i]= new Tag(tagsStringArray[i]);
        }

        Pet pet = new Pet();
        pet.setId(Integer.parseInt(petSpecs.get(0)));
        pet.setCategory(new Category(petSpecs.get(1)));
        pet.setName(petSpecs.get(2));
        pet.setPhotoUrls(photoUrls);
        pet.setTags(tags);
        pet.setStatus(petSpecs.get(5));

        petService.addPet(session,pet);
    }
}
