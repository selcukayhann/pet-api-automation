package steps;

import api.UserService;
import net.thucydides.core.annotations.Steps;
import io.cucumber.java.en.*;

public class UserServiceStepDefinitions {

    @Steps
    private Session session;

    @Steps
    private UserService userService;

    public UserServiceStepDefinitions(Session session){
        this.session=session;
    }

    public UserServiceStepDefinitions(){
    }


    @Given("^a valid user exists$")
    public void a_valid_user_exists() {
        session.setRequest(userService.getRequestWithJSONHeaders());
        session.setResponse(userService.createUser(session.getRequest()));
        userService.verifyResponseStatusValue(session.getResponse(), userService.SUCCESS_STATUS_CODE);
    }

    @When("I search for the user by their username$")
    public void i_search_for_the_user_by_their_username() {
        session.setResponse(userService.getUserByUsername(session.getRequest()));
    }

    @Then("the user is located$")
    public void the_user_is_located() {

    }
}
