package steps;

import api.OrderService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import model.Order;
import net.thucydides.core.annotations.Steps;

public class OrderServiceStepDefinitions {

    @Steps
    private Session session;

    @Steps
    private OrderService orderService;

    public OrderServiceStepDefinitions(Session session) {
        this.session = session;
    }

    public OrderServiceStepDefinitions(){}

    @When("^I search for an order with an id value of '(.*?)'")
    public void i_search_for_an_order_with_an_id_value_of_id(String id) {
        session.setResponse(orderService.getOrderById(session.getRequest(),id));
    }

    @When("^I place an order for a pet with an order id of '(\\d+)'$")
    public void i_make_a_post_request(Integer orderId) {
        session.setRequest(orderService.getRequestWithJSONHeaders());
        session.setResponse(orderService.placeOrder(session.getRequest(), new Order(orderId, 0, 1, "2019-02-05T14:11:44.922Z", "placed", false)));
    }

    @When("^I get the endpoint (.*?)$")
    public void i_hit_the_endpoint(String endpointPath) {

    }

    @Then("^the order request response has a '(\\d+)' response code$")
    public void the_response_has_the_correct_response_code(Integer rc) {
        orderService.verifyResponseStatusValue(session.getResponse(), rc.intValue());
    }

    @Then("^the order requests response contains the correct json data$")
    public void the_json_response_contains_the_correct_data() {
        orderService.verifyResponseKeyValues("id", "58", session.getResponse());
    }

    @Given("^an order exists for a pet$")
    public void an_order_exists_for_a_pet() {
        session.setRequest(orderService.getRequestWithJSONHeaders());
        session.setResponse(orderService.placeOrder(session.getRequest()));
        orderService.verifyResponseStatusValue(session.getResponse(), orderService.SUCCESS_STATUS_CODE);
    }

    @When("^I search for the order by its id$")
    public void i_search_for_the_order_by_its_id() {
        session.setResponse(orderService.getOrderById(session.getRequest(), orderService.getDefaultOrder().getId().toString()));
    }

    @Then("^the complete order is returned$")
    public void the_complete_order_is_returned() {
        orderService.verifyOrderValuesAreAsExpected(session.getResponse(), orderService.getDefaultOrder());
    }
}
