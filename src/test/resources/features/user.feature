Feature: User Tests

  @Test
  Scenario: Users are searchable by their username
    Given a valid user exists
    When I search for the user by their username
    Then the user is located
