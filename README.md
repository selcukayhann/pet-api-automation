# pet-api-automation
pet-api-automation with Serenity

# To build and Run
Please type following command to the project main directory(same directory with pom.xml)

-mvn clean test

# To have detailed test report after run
Please type following command to the project main directory(same directory with pom.xml)

-mvn serenity:aggregate

Then you can find the report at target/site/serenity/index.html

# Structure

BaseService is our Parent service which defines and sets all generic service processes.

Foreach service, we have service instances which inherits BaseService. For example : PetService, OrderService...

Session.java is our object for storing and retrieving session data

Model package is storing our java object models. 

Step definitions divided depending on service type and placed under steps package.

Features divided depending on service type and places under resources/feature.

# Creating test case

For Particular service;

- Create a feature file under resource/features
- Create a step definition file under steps.
- Create your service object which extends BaseService
- Create your models if it needed
- While creating test steps to performing requests use Session to store your request and response for future use.
  
  Here is an example with dummy methods : 
  
      session.setRequest(service.getRequestSpecs());
      session.setResponse(service.performSpecificRequest());
 
  Your step definition for performing requests will look like : 

      @When("^I perform request$")
        public void i_perform_request() {
              session.setRequest(service.getRequestSpecs());
              session.setResponse(service.performSpecificRequest());
        }
        
- While creating test steps to make validations use stored Session values for your particular actions.

        @Then("^I make validations$")
        public void i_make_validations() {
            service.verifySpecificData(session.getResponse());
        }
        
  Your service function will perform the parsing and validation as following :
        
         public void verifySpecificData(Response response) {
             String value = response.jsonPath().getString("spec");
             verifyTrue(!idVal.equalsIgnoreCase(SPEC_TO_VALIDATE));
         }
         
- For particular service instance :
    
    - Define your endpoint as final String
    - Define your specific object instances
    - Define your default constructor without any parameter
    - In constructor method init your default object instances with default values
    - Use Parent sendRequest method for your request methods
    
  Your service class will look like : 
  
        public class exampleService extends BaseService {
            private final String SERVICE_ENDPOINT_PATH = "service/";
            private Pet defaultServiceObjectModel;
        
            public PetService() {
                super();
                defaultServiceObjectModel = new defaultServiceObjectModel();
            }
            
- Performing request is really simple with sendRequest() method :

    - Define your request specifications and use sendRequest() to perform requests
    - First we should pass RequestSpecification
    - Second we should pass Request types which statically defined in BaseService as GET_REQUEST, POST_REQUEST, DELETE_REQUEST, PUT_REQUEST
    - Third is url which defined in service objects as final 
    - Fourth is request body if it is needed.
    - If request is empty sendRequest is directly perform get request to the passed url
    - Store your models and responses to by the related objects
    
   Here is an example usage of sendRequest() method for post request :
    
        sendRequest(request, BaseService.POST_REQUEST, url, bodyString);
          
- Performing validation depends on parsing stored response and comparing with stored model objects
    
    - Pass stored response from the Session and the object you want to compare to the verification method
    
          service.verifySomething(session.getResponse(), service.getStoredServiceObject());
          
    - Parse and validate the data as you need 
    
          public void verifySpecificKey(Response response, StoredServiceObject object) {
             String KeyValue = response.jsonPath().getString(KEY);
             assertThat(KeyValue, object.getKeyValue);            
             }
            